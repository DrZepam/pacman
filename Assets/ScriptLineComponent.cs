﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptLineComponent : MonoBehaviour {
	public bool instanciate = false;
	public float pas;
	public float invert = 1f;
	public List<Sprite> spr; 
	// Use this for initialization
	void Start () {
		if(spr.Count>0)
			GetComponent<SpriteRenderer> ().sprite = spr[Random.Range(0,spr.Count)];
		if (!instanciate)
			return;
		for (int i = 1; i < 10; i++) {
			GameObject go = Instantiate (gameObject) as GameObject;
			go.GetComponent<ScriptLineComponent> ().instanciate = false;
			go.transform.position = transform.position;
			go.transform.position = new Vector3 (go.transform.position.x, go.transform.position.y + i * pas, go.transform.position.z);
		}
		for (int i = 1; i < 10; i++) {
			GameObject go = Instantiate (gameObject) as GameObject;
			go.GetComponent<ScriptLineComponent> ().instanciate = false;
			go.transform.position = transform.position;
			go.transform.position = new Vector3 (go.transform.position.x, go.transform.position.y + i * pas * -1, go.transform.position.z);
		}
//		GameObject go = Instantiate (gameObject) as GameObject;
//		go.GetComponent<ScriptLineComponent> ().instanciate = false;
//		go.transform.position = transform.position;
//		go.transform.position = new Vector3 (go.transform.position.x, go.transform.position.y + 59, go.transform.position.z);
//		go = Instantiate (gameObject) as GameObject;
//		go.transform.position = transform.position;
//		go.transform.position = new Vector3 (go.transform.position.x, go.transform.position.y - 59, go.transform.position.z);

	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.up * 0.1f * invert);
		if (transform.localPosition.y > 23 && invert > 0) {
			transform.position = new Vector3(transform.position.x,-46,transform.position.z);
		}
			else if (transform.localPosition.y < -23 && invert < 0) {
				transform.position = new Vector3(transform.position.x,46,transform.position.z);
			}
	}
}
